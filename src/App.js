import React from 'react';
import {
  BrowserRouter as Router, Route, Switch
} from 'react-router-dom';
import { Container } from '@material-ui/core';
import HomePage from './views/Cars/HomePage';
import * as ROUTES from './constants/routes';
import './App.css';

const App = () => {

  return (
    <Container className='app-container'>
      <Router>
        <Switch>
          <Route component={HomePage} path={ROUTES.HOME_PAGE} exact />
        </Switch>
      </Router>
    </Container>
  )
};

export default App;
