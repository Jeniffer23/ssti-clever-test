import React from 'react';
import { Typography } from '@material-ui/core';

const TitleModule = ({ title }) => {

    return (
        <div style={{ width: '100%', marginBottom: '2%' }}>
            <Typography style={{
                color: '#ffff',
                fontWeight: 'normal',
                fontSize: '27.9915px',
                lineHeight: '39px',
                backgroundColor: '#231E60',
                display: 'flex',
                justifyContent: 'center'

            }} noWrap>
                {title}
            </Typography>
        </div>
    )
}

export default TitleModule;