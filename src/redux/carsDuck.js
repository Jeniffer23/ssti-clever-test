import { firestore } from '../services/Firebase';

// constants
const initialState = {
    cars: [],
    car: {},
    isEditedCreated: false,
    isCreating: true,
    colors: []
};
var unsubscribe = null;

// types
const LOADING_CAR = 'LOADING_CAR';
const CAR_ERROR = 'CAR_ERROR';
const IS_CREATING_CAR = 'IS_CREATING_CAR';
const GET_CARS_SUCCESS = 'GET_CARS_SUCCESS';
const COLORS_SUCCESS = 'COLORS_SUCCESS';
const CAR_CREATE_UPDATE = 'CAR_CREATE_UPDATE';
const GET_CAR_BY_ID_SUCCESS = 'GET_CAR_BY_ID_SUCCESS';
const CANCEL_CREATED_UPDATED_CAR = 'CANCEL_CREATED_UPDATED_CAR';

// reducer
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case LOADING_CAR:
            return { ...state, loading: true, error: null };
        case IS_CREATING_CAR:
            return {
                ...state, isCreating: action.payload,
                isEditedCreated: false,
            };
        case GET_CARS_SUCCESS:
            return {
                ...state, cars: action.payload,
                error: null, loading: false, car: {}
            };
        case COLORS_SUCCESS:
            return {
                ...state, colors: action.payload,
                loading: false, error: null
            };
        case CAR_CREATE_UPDATE:
            return {
                ...state, car: {}, isEditedCreated: true,
                loading: false, error: null
            };
        case CANCEL_CREATED_UPDATED_CAR:
            return {
                ...state, isEditedCreated: false
            };
        case GET_CAR_BY_ID_SUCCESS:
            return {
                ...state, car: action.payload,
                loading: false, error: null, isCreating: false
            };
        case CAR_ERROR:
            return {
                ...state, error: action.payload, loading: false
            };
        default:
            return state;
    }

}

//Actions

export const changeIsCreating = (state) => (dispatch) => {
    dispatch({
        type: IS_CREATING_CAR,
        payload: state
    });
};

export const getCarList = () => async (dispatch) => {
    // dispatch({ type: LOADING_CAR });
    try {
        if (unsubscribe)
            unsubscribe();
        unsubscribe =
            firestore.collection('cars/').onSnapshot(querySnapshot => {
                dispatch({
                    type: GET_CARS_SUCCESS,
                    payload: onCollectionUpdate(querySnapshot)
                });
            });
    } catch (error) {
        dispatch({
            type: CAR_ERROR,
            payload: 'Error al cargar lista de carros'
        });
    }
};

const onCollectionUpdate = (querySnapshot) => {
    const carsList = [];

    querySnapshot.forEach(doc => {
        const data = doc.data();

        carsList.push({
            key: doc.id,
            complete: data.value
        });
    });
    return carsList;
};


export const getColors = () => async (dispatch) => {
    try {
        const ColorsList = [];
        const Colors = await firestore.collection('colors').get();
        Colors.forEach(item => {
            ColorsList.push({
                key: item.data().name,
                value: item.data().name
            });
        });
        dispatch({
            type: COLORS_SUCCESS,
            payload: ColorsList
        })
    } catch (error) {
        dispatch({
            type: CAR_ERROR,
            payload: 'Error al cargar los colores'
        })
    }
};

export const addDataCars = (car) => async (dispatch, getState) => {
    try {
        const { isCreating } = getState().car;
        const carRedux = getState().car.car;

        const newCar = {
            value: `${car.plaque} | ${car.model} | ${parseFloat(car.price)} | ${car.color} | 
            ${car.status === 'new' ? 'Nuevo' : 'Usado'} | ${car.active === true ? 'Activo' : 'Desactivado'}`
        }
        if (isCreating) {
            await firestore.collection('cars').add(newCar);
        } else
            await firestore.doc(`cars/${carRedux.key}`).update(newCar);

        dispatch({
            type: CAR_CREATE_UPDATE,
            payload: "Usuario agregado correctamente"
        });
    } catch (error) {
        dispatch({
            type: CAR_ERROR,
            payload: "Error al agregar el nuevo carro"
        });
    }
}

export const cancelCarCreatedUpdated = () => (dispatch) => {
    dispatch({
        type: CANCEL_CREATED_UPDATED_CAR,
    });
};

export const splitCar = (car) => (dispatch) => {
    try {
        dispatch({ type: IS_CREATING_CAR, payload: true });
        const dataSplit = car.complete.split(' | ');
        const carObject = {
            key: car.key,
            plaque: dataSplit[0],
            model: dataSplit[1],
            price: dataSplit[2],
            color: dataSplit[3],
            status: dataSplit[4] === 'Nuevo' ? 'new' : 'used',
            active: dataSplit[5] === 'Activo' ? true : false
        };
        dispatch({
            type: GET_CAR_BY_ID_SUCCESS,
            payload: carObject
        });
    } catch (error) {
        dispatch({
            type: CAR_ERROR,
            payload: 'Error al cargar el carro'
        });
    }
};