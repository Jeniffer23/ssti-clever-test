import thunk from 'redux-thunk';
import {
    createStore, combineReducers, compose, applyMiddleware
} from 'redux';
import carsReducer from './carsDuck';


const rootReducer = combineReducers({
    car: carsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore() {
    const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
    return store;
};