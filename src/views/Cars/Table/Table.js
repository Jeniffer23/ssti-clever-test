import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getCarList, splitCar } from '../../../redux/carsDuck';
import { withRouter } from 'react-router-dom';
import {
    Button, Table, Paper, TableRow,
    TableHead, TableContainer, TableBody, TableCell
} from '@material-ui/core';



const TableList = (props) => {
    const carsList = useSelector(store => store.car.cars);

    const [data, setData] = useState([]);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getCarList());
    }, [dispatch]);

    useEffect(() => {
        setData(carsList);
    }, [carsList]);

    const splitCarObject = (row) => {
        dispatch(splitCar(row));
    };

    return (
        <TableContainer component={Paper} style={{
            width: '90%', margin: 30
        }}>
            <Table aria-label="customized table">
                <TableHead style={{
                    backgroundColor: 'black', color: 'white',
                }}>
                    <TableRow>
                        <TableCell style={{ color: 'white' }}>Información</TableCell>
                        <TableCell style={{ color: 'white' }} >Editar</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((row, index) => (
                        <TableRow
                            key={index}
                        >
                            <TableCell component="th" scope="row">
                                {row.complete}
                            </TableCell>
                            <TableCell component="th" scope="row">
                                <Button aria-label='edit' variant='outlined' onClick={() => splitCarObject(row)}>
                                    Editar
                                </Button>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer >
    );
}

export default withRouter(TableList);